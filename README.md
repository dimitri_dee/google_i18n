# Google I18n Address

This gem provides I18n address normalization and validation based on
[Google's i18n address repository](https://chromium-i18n.appspot.com/ssl-address).

Code was ported to Ruby from this 
[Python package](https://github.com/mirumee/google-i18n-address).


## Installation

Add this line to your application's Gemfile:

```ruby
gem 'google_i18n'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install google_i18n

## Usage

Initialize with some combination of the following:
 - `country_code` The two-letter ISO 3166-1 country code
 - `street_address` The street address
 - `city` The city or town
 - `city_area` The sublocality or district
 - `name` A person's name
 - `company_name` The name of a company or organization
 - `country_area` The region, province or state
 - `sorting_code` The sorting code
 - `postal_code` The postal code or zip code

### Normalization

```
address = {country_code: 'US', country_area: 'California', city: 'Mountain View', postal_code: '94043', street_address: '1600 Amphitheatre Pkwy'}
a = GoogleI18n::Address.new(address)
a.normalize
 => #<GoogleI18n::Address @country_code="US", @country_area="CA", @city="MOUNTAIN VIEW", @city_area="", @postal_code="94043", @street_address="1600 Amphitheatre Pkwy", @sorting_code="">
a.to_s
 => "1600 Amphitheatre Pkwy\nMOUNTAIN VIEW , CA 94043\nUNITED STATES"
a.field_order
 => [["name"], ["company_name"], ["street_address"], ["city", "country_area", "postal_code"], ["country_name"]]

address = {country_code: 'CN', 'country_area': 'Beijing', 'city': 'Chaoyang', 'postal_code': '100600', street_address: '55 Anjialou Rd'}
GoogleI18n::Address.new(address).normalize
 => #<GoogleI18n::Address @country_code="CN", @street_address="55 Anjialou Rd", @city="朝阳区", @country_area="北京市", @postal_code="100600", @city_area="", @sorting_code="">
```

### Latinization

```
address = {'country_code': 'CN', 'country_area': '北京市', 'city': '朝阳区', 'city_area': '三元桥', 'street_address': '安家楼路55号', 'postal_code': '100600'}
GoogleI18n::Address.new(address).latinize
 => #<GoogleI18n::Address @country_code="CN", @country_area="Beijing Shi", @city="Chaoyang Qu", @city_area="三元桥", @postal_code="100600", @street_address="安家楼路55号", @sorting_code="">
GoogleI18n::Address.new(address).latinize.normalize
 => #<GoogleI18n::Address @country_code="CN", @street_address="55 Anjialou Rd", @city="朝阳区", @country_area="北京市", @postal_code="100600", @city_area="", @sorting_code="">
```

### Validation

```
GoogleI18n::Address.new({country_code: 'US'}).normalize
 => GoogleI18n::InvalidAddress (Invalid address {"country_area"=>"required", "city"=>"required", "postal_code"=>"required", "street_address"=>"required"})
```

### Country Areas

```
GoogleI18n::Address.new({country_code: 'US'}).country_area_choices
 => [['AL', 'Alabama'], ..., ['WY', 'Wyoming']]

```

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitLab at https://gitlab.com/dimitridee/google_i18n
