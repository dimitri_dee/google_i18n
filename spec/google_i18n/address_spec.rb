RSpec.describe GoogleI18n::Address do
  it "has a version number" do
    expect(GoogleI18n::VERSION).not_to be nil
  end

  # let(:address_us) do
  #   {'country_code': 'US','country_area': 'California','city': 'Mountain View','postal_code': '94043','street_address': '1600 Amphitheatre Pkwy'}
  # end
  let(:address_us) do
    {country_code: 'US', country_area: 'California', city: 'Mountain View', postal_code: '94043', street_address: '1600 Amphitheatre Pkwy'}
  end

  let(:address_cn) do
    {'country_code': 'CN', 'country_area': '北京市', 'city': '朝阳区', 'city_area': '三元桥', 'street_address': '安家楼路55号', 'postal_code': '100600', 'company_name': '美国驻华大使馆', 'name': '陈港生'}
  end

  let(:address_cn_latin) do
    {country_code: 'CN', 'country_area': 'Beijing', 'city': 'Chaoyang', 'city_area': 'San Yuan Qiao', 'street_address': '55 Anjialou Rd', 'postal_code': '100600', 'company_name': 'U.S. Embassy', 'name': 'Jackie Chan'}
  end

  describe '#dup' do
    subject { GoogleI18n::Address.new(address).dup }
    let(:address) { address_us }
    specify { expect(subject).to have_attributes(address_us) }
    specify { expect(subject).to be_instance_of(described_class) }
  end

  describe '#to_h' do
    subject { GoogleI18n::Address.new(address).to_h }
    let(:address) { address_us }
    specify { expect(subject).to include(address_us) }
    specify { expect(subject).to be_instance_of(Hash) }
  end

  describe "#to_s" do
    subject { GoogleI18n::Address.new(address).to_s(opts) }
    let(:opts) { {} }

    context 'address is chinese' do
      let(:address) { address_cn }
      let(:expected) { "100600\n北京市 朝阳区 三元桥\n安家楼路55号\n美国驻华大使馆\n陈港生\nCHINA" }
      specify { expect(subject).to eq(expected) }

      context 'when latin field order is requested' do
        before do
          opts.merge!({latin: true})
        end
        let(:expected) { "陈港生\n美国驻华大使馆\n安家楼路55号\n三元桥\n朝阳区\n北京市 , 100600\nCHINA" }
        specify { expect(subject).to eq(expected) }
      end
    end

    context 'address is latinized chinese' do
      let(:address) { address_cn_latin }
      let(:expected) { "100600\n北京市 朝阳区 San Yuan Qiao\n55 Anjialou Rd\nU.S. Embassy\nJackie Chan\nCHINA" }
      specify { expect(subject).to eq(expected) }

      context 'without normalization' do
        before do
          opts.merge!({normalize: false})
        end
        let(:expected) { "100600\nBEIJING Chaoyang San Yuan Qiao\n55 Anjialou Rd\nU.S. Embassy\nJackie Chan\nCHINA" }
        specify { expect(subject).to eq(expected) }
      end
    end
  end

  describe "#field_order" do
    subject { GoogleI18n::Address.new(address).field_order(opts) }
    let(:opts) { {} }

    context 'address is chinese' do
      let(:address) { address_cn }
      let(:expected) { [["postal_code"], ["country_area", "city", "city_area"], ["street_address"], ["company_name"], ["name"], ["country_name"]] }
      specify { expect(subject).to eq(expected) }

      context 'when latin field order is requested' do
        before do
          opts.merge!({latin: true})
        end
        let(:expected) { [["name"], ["company_name"], ["street_address"], ["city_area"], ["city"], ["country_area", "postal_code"], ["country_name"]] }
        specify { expect(subject).to eq(expected) }
      end
    end

    context 'address is american' do
      let(:address) { address_us }
      let(:expected) { [["name"], ["company_name"], ["street_address"], ["city", "country_area", "postal_code"], ["country_name"]] }
      specify { expect(subject).to eq(expected) }
    end
  end

  describe "#normalize" do
    subject { GoogleI18n::Address.new(address).normalize }

    context 'address is chinese' do
      let(:address) { address_cn }
      specify { expect(subject).to have_attributes(address_cn) }
      specify { expect(subject).to be_instance_of(described_class) }
    end

    context 'address is latinized chinese' do
      let(:address) { address_cn_latin }

      # the metadata does not provide local versions of certain specifics
      specify do
        expect(subject).to have_attributes(address_cn.merge({
          city_area: address_cn_latin[:city_area],
          street_address: address_cn_latin[:street_address],
          company_name: address_cn_latin[:company_name],
          name: address_cn_latin[:name]
        }))
      end
      specify { expect(subject).to be_instance_of(described_class) }
    end

    context 'address is american' do
      let(:address) { address_us }

      # certain fields may be capitalized or abbreviated depending on locale
      specify { expect(subject).to have_attributes(address_us.merge({city: "MOUNTAIN VIEW", country_area: "CA"})) }
      specify { expect(subject).to be_instance_of(described_class) }
    end
  end

  describe "#latinize" do
    subject { GoogleI18n::Address.new(address).latinize(opts) }
    let(:opts) { {} }

    context 'address is chinese' do
      let(:address) { address_cn }
      specify { expect(subject).to have_attributes(address_cn.merge({city: "Chaoyang Qu", country_area: "Beijing Shi"})) }
      specify { expect(subject).to be_instance_of(described_class) }
    end

    context 'address is american' do
      let(:address) { address_us }

      # certain fields may be capitalized or abbreviated depending on locale
      specify { expect(subject).to have_attributes(address_us.merge({city: "MOUNTAIN VIEW"})) }
      specify { expect(subject).to be_instance_of(described_class) }

      context 'without normalization' do
        before do
          opts.merge!({normalize: false})
        end
        specify { expect(subject).to have_attributes(address_us) }
        specify { expect(subject).to be_instance_of(described_class) }
      end
    end
  end
end

