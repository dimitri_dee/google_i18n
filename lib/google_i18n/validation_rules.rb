module GoogleI18n
  class ValidationRules
    attr_reader :country_code, :country_name, :address_format, :address_latin_format, :allowed_fields,
                :required_fields, :upper_fields, :country_area_type, :country_area_choices, :city_type,
                :city_choices, :city_area_type, :city_area_choices, :postal_code_type, :postal_code_matchers,
                :postal_code_examples, :postal_code_prefix

    def initialize(country_code, country_name, address_format, address_latin_format, allowed_fields,
                   required_fields, upper_fields, country_area_type, country_area_choices, city_type,
                   city_choices, city_area_type, city_area_choices, postal_code_type, postal_code_matchers,
                   postal_code_examples, postal_code_prefix)
      @country_code = country_code
      @country_name = country_name
      @address_format = address_format
      @address_latin_format = address_latin_format
      @allowed_fields = allowed_fields
      @required_fields = required_fields
      @upper_fields = upper_fields
      @country_area_type = country_area_type
      @country_area_choices = country_area_choices
      @city_type = city_type
      @city_choices = city_choices
      @city_area_type = city_area_type
      @city_area_choices = city_area_choices
      @postal_code_type = postal_code_type
      @postal_code_matchers = postal_code_matchers
      @postal_code_examples = postal_code_examples
      @postal_code_prefix = postal_code_prefix
    end
  end
end