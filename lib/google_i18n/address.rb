module GoogleI18n
  class Address
    VALID_COUNTRY_CODE = %r(^\w{2,3}$)
    VALIDATION_DATA_DIR = File.join(File.dirname(__FILE__), "data")
    VALIDATION_DATA_PATH = File.join(VALIDATION_DATA_DIR, "%s.json")

    FIELD_MAPPING = {
      "A" => "street_address",
      "C" => "city",
      "D" => "city_area",
      "N" => "name",
      "O" => "company_name",
      "S" => "country_area",
      "X" => "sorting_code",
      "Z" => "postal_code",
    }

    KNOWN_FIELDS = FIELD_MAPPING.values.push("country_code")

    attr_accessor :country_code, :street_address, :city, :city_area, :name, :company_name, 
                  :country_area, :sorting_code, :postal_code

    # Creates a new Address object from component parts
    # 
    # @option options [String] :country_code The two-letter ISO 3166-1 country code
    # @option options [String] :street_address The street address
    # @option options [String] :city The city or town
    # @option options [String] :city_area The sublocality or district
    # @option options [String] :name A person's name
    # @option options [String] :company_name The name of a company or organization
    # @option options [String] :country_area The region, province or state
    # @option options [String] :sorting_code The sorting code
    # @option options [String] :postal_code The postal code or zip code
    # 
    # @return [GoogleI18n::Address] The constructed Address
    def initialize(options={})
      @country_code = options[:country_code] if options[:country_code]
      @street_address = options[:street_address] if options[:street_address]
      @city = options[:city] if options[:city]
      @city_area = options[:city_area] if options[:city_area]
      @name = options[:name] if options[:name]
      @company_name = options[:company_name] if options[:company_name]
      @country_area = options[:country_area] if options[:country_area]
      @sorting_code = options[:sorting_code] if options[:sorting_code]
      @postal_code = options[:postal_code] if options[:postal_code]
    end

    def dup
      self.class.new(
        country_code: self.country_code ? self.country_code.dup : nil,
        street_address: self.street_address ? self.street_address.dup : nil,
        city: self.city ? self.city.dup : nil,
        city_area: self.city_area ? self.city_area.dup : nil,
        name: self.name ? self.name.dup : nil,
        company_name: self.company_name ? self.company_name.dup : nil,
        country_area: self.country_area ? self.country_area.dup : nil,
        sorting_code: self.sorting_code ? self.sorting_code.dup : nil,
        postal_code: self.postal_code ? self.postal_code.dup : nil,
      )
    end

    def to_h
      {
        country_code: self.country_code,
        street_address: self.street_address,
        city: self.city,
        city_area: self.city_area,
        name: self.name,
        company_name: self.company_name,
        country_area: self.country_area,
        sorting_code: self.sorting_code,
        postal_code: self.postal_code,
      }
    end

    # Displays the address in a newline delimited format suitable for printing
    # 
    # @param normalize [Boolean] If true, address will be normalized first
    # @param latin [Boolean] Show the address fields in Latinized order
    # 
    # @return [String]
    def to_s(normalize: true, latin: false)
      cleaned_data = address.dup
      cleaned_data = cleaned_data.normalize if normalize

      rules = get_validation_rules

      address_format = if latin
        rules.address_latin_format
      else
        rules.address_format
      end

      address_line_formats = address_format.split("%n")

      address_lines = address_line_formats.inject([]) do |m, line_format|
        m << format_address_line(line_format, cleaned_data, rules)
        m
      end

      address_lines << rules.country_name
      address_lines.keep_if(&:present?)
      address_lines.join("\n")
    end

    # Returns an array where each element is an array representing the fields to include on each address line, e.g:
    #   [["name"], ["company_name"], ["street_address"], ["city", "country_area", "postal_code"], ["country_name"]]
    # 
    # @param latin [Boolean] Show the address fields in Latinized order
    # 
    # @return [Array<Array<String>>]
    def field_order(latin: false)
      rules = get_validation_rules

      address_format = if latin
        rules.address_latin_format
      else
        rules.address_format
      end

      address_lines = address_format.split("%n")

      replacements = FIELD_MAPPING.inject({}) do |m, (code, field_name)|
        m["%#{code}"] = field_name
        m
      end

      all_lines = []
      address_lines.each do |line|
        fields = line.split(%r{(%.)})
        single_line = fields.map {|field| replacements[field]}.compact
        all_lines << single_line
      end

      all_lines << Array('country_name')
    end

    # Returns an array where each element is an array representing the key and value for each country area. e.g:
    #   [['AL', 'Alabama'], ..., ['WY', 'Wyoming']]
    def country_area_choices
      get_validation_rules.country_area_choices
    end

    # Format the address in a way suitable for use in its part of the world
    # 
    # @return [GoogleI18n::Address]
    def normalize
      errors = {}
      # begin
        rules = get_validation_rules
      # rescue StandardError
      #   errors["country_code"] = "invalid"
      # end

      if errors.blank?
        cleaned_data = address.dup
        country_code = cleaned_data.country_code
        if country_code.blank?
          errors["country_code"] = "required"
        else
          cleaned_data.country_code = country_code.upcase
        end

        cleaned_data.country_area, errors = normalize_field("country_area", rules, cleaned_data, rules.country_area_choices, errors)
        cleaned_data.city, errors = normalize_field("city", rules, cleaned_data, rules.city_choices, errors)
        cleaned_data.city_area, errors = normalize_field("city_area", rules, cleaned_data, rules.city_area_choices, errors)
        cleaned_data.postal_code, errors = normalize_field("postal_code", rules, cleaned_data, [], errors)

        postal_code = cleaned_data.postal_code
        if rules.postal_code_matchers.present? && postal_code.present?
          rules.postal_code_matchers.each do |matcher|
            if !matcher.match(postal_code)
              errors["postal_code"] = "invalid"
              break
            end
          end
        end

        cleaned_data.street_address, errors = normalize_field("street_address", rules, cleaned_data, [], errors)
        cleaned_data.sorting_code, errors = normalize_field("sorting_code", rules, cleaned_data, [], errors)
        cleaned_data
      end

      if errors.present?
        raise InvalidAddress, "Invalid address #{errors}"
      end

      cleaned_data
    end

    # Format the address in a way suitable for use in Latin alphabets.
    #   If a latinized version is not available, the local version will be used instead.
    # 
    # @param normalize [Boolean] If true, address will be normalized first
    # 
    # @return [GoogleI18n::Address]
    def latinize(normalize: true)
      cleaned_data = address.dup
      cleaned_data = cleaned_data.normalize if normalize

      country_code = cleaned_data.country_code&.upcase
      dummy_country_data, database = load_country_data(country_code)

      if country_code.present?
        country_area = cleaned_data.country_area
        if country_area.present?
          key = "%s/%s" % [country_code, country_area]
          country_area_data = database[key]
          if country_area_data.present?
            cleaned_data.country_area = country_area_data.fetch("lname", country_area_data.fetch("name", country_area))
            city = cleaned_data.city
            key = "%s/%s/%s" % [country_code, country_area, city]
            city_data = database[key]
            if city_data.present?
              cleaned_data.city = city_data.fetch("lname", city_data.fetch("name", city))
              city_area = cleaned_data.city_area
              key = "%s/%s/%s/%s" % [country_code, country_area, city, city_area]
              city_area_data = database[key]
              if city_area_data.present?
                cleaned_data.city_area = city_area_data.fetch("lname", city_area_data.fetch("name", city_area))
              end
            end
          end
        end
      end

      cleaned_data
    end

    private

    def address
      self
    end

    def format_address_line(line_format, address, rules)
      replacements = FIELD_MAPPING.inject({}) do |m, (code, field_name)|
        value = address.send(field_name)
        value = value.upcase if rules.upper_fields.include?(field_name)
        m["%#{code}"] = value
        m
      end

      fields = line_format.split(%r{(%.)})
      fields = fields.map {|field| replacements.fetch(field, field)&.strip}
      fields.keep_if(&:present?)
      fields.join(' ')
    end

    def normalize_field(name, rules, data, choices, errors)
      value = data.send(name)

      if rules.upper_fields.include?(name) && value.present?
        data.send("#{name}=", value.upcase)
      end

      if !rules.allowed_fields.include?(name)
        data.send("#{name}=", "")
      elsif value.blank?
        if rules.required_fields.include?(name)
          errors[name] = "required"
        else
          data.send("#{name}=", "")
        end
      elsif choices.present?
        if value.present? || rules.required_fields.include?(name)
          value = match_choices(value, choices)
          if value.present?
            data.send("#{name}=", value)
          else
            errors[name] = "invalid"
          end
        end
      end

      [data.send(name), errors]
    end

    def make_choices(rules, translated = false)
      sub_keys = rules["sub_keys"]
      return if sub_keys.blank?

      choices = []
      sub_keys = sub_keys.split("~")

      if sub_names = rules["sub_names"]
        choices += sub_keys.zip(sub_names.split("~"))
      elsif !translated
        choices += sub_keys.zip(sub_keys)
      end

      if !translated
        if sub_lnames = rules["sub_lnames"]
          choices += sub_keys.zip(sub_lnames.split("~"))
        end

        if sub_lfnames = rules["sub_lfnames"]
          choices += sub_keys.zip(sub_lfnames.split("~"))
        end
      end

      choices
    end

    # This returns the regional form of `value`.
    # `choices` looks something like this: [['安徽省', 'Anhui Sheng'], ['安徽省', '安徽省']]
    # the idea is that whether `value` is 'Anhui Sheng' or '安徽省', it will return '安徽省'
    def match_choices(value, choices)
      return if value.blank?
      value = value.strip.downcase
      # using `match` here rather than `==` because we want them to be able to use "New York" and "New York City"
      matched = choices.select { |name, label| name&.downcase.to_s.match(value) || label&.downcase.to_s.match(value) }
      matched.flatten.first
    end

    # This returns something like:
    # [['AL', 'Alabama'], ['AK', 'Alaska'], ['AS', 'American Samoa'], ['AZ', 'Arizona'], ...]
    # or for non-latin alphabets:
    # [['安徽省', 'Anhui Sheng'], ['安徽省', '安徽省'], ['澳门', 'Macau'], ['澳门', '澳门'], ['北京市', 'Beijing Shi'], ['北京市', '北京市'], ...]
    def compact_choices(choices)
      value_map = ActiveSupport::OrderedHash.new {|hsh, key| hsh[key] = [] }
      choices.each do |key, value|
        value_map[key] << value.to_s
      end

      ret = []
      value_map.each do |k,vv|
        vv.sort.each do |v|
          ret << [k,v]
        end
      end

      ret
    end

    def get_validation_rules
      country_code = address.country_code&.upcase
      country_data, database = load_country_data(country_code)
      country_name = country_data.fetch("name", "")
      address_format = country_data["fmt"]
      address_latin_format = country_data.fetch("lfmt", address_format)

      allowed_fields = address_format.scan(%r{%([#{FIELD_MAPPING.keys.join}])}).flatten
      allowed_fields = FIELD_MAPPING.fetch_values(*allowed_fields)
      required_fields = FIELD_MAPPING.fetch_values(*country_data["require"].split(//))
      upper_fields = FIELD_MAPPING.fetch_values(*country_data["upper"].split(//))

      languages = country_data["languages"]&.split("~") || []

      postal_code_matchers = []
      if allowed_fields.include?("postal_code") && country_data.has_key?("zip")
        postal_code_matchers << %r(^#{country_data["zip"]}$)
      end

      postal_code_examples = country_data["zipex"]&.split(",") || []

      country_area_choices = []
      city_choices = []
      city_area_choices = []
      country_area_type = country_data["state_name_type"]
      city_type = country_data["locality_name_type"]
      city_area_type = country_data["sublocality_name_type"]
      postal_code_type = country_data["zip_name_type"]
      postal_code_prefix = country_data.fetch("postprefix", "")

      # second level of data is for administrative areas
      country_area = nil
      city = nil
      city_area = nil
      if database.has_key?(country_code)
        if country_data.has_key?("sub_keys")
          languages.each do |language|
            is_default_language = (language.blank? || language == country_data["lang"])
            matched_country_area = nil
            matched_city = nil
            if is_default_language
              localized_country_data = database[country_code]
            else
              localized_country_data = database["#{country_code}--#{language}"]
            end

            localized_country_area_choices = make_choices(localized_country_data)
            country_area_choices += localized_country_area_choices
            existing_choice = country_area.present?
            matched_country_area = country_area = match_choices(address.country_area, localized_country_area_choices)

            # third level of data is for cities
            if matched_country_area.present?
              if is_default_language
                country_area_data = database["#{country_code}/#{country_area}"]
              else
                country_area_data = database["#{country_code}/#{country_area}--#{language}"]
              end

              if existing_choice.blank?
                if country_area_data.include?("zip")
                  postal_code_matchers << %r(^#{country_area_data["zip"]})
                end

                if country_area_data.include?("zipex")
                  postal_code_examples = country_area_data["zipex"].split(",")
                end
              end

              if country_area_data.include?("sub_keys")
                localized_city_choices = make_choices(country_area_data)
                city_choices += localized_city_choices
                existing_choice = city.present?
                matched_city = city = match_choices(address.city, localized_city_choices)
              end

              # fourth level of data is for dependent sublocalities
              if matched_city.present?
                if is_default_language
                  city_data = database["#{country_code}/#{country_area}/#{city}"]
                else
                  city_data = database["#{country_code}/#{country_area}/#{city}--#{language}"]
                end

                if existing_choice.blank?
                  if city_data.include?("zip")
                    postal_code_matchers << %r(^#{city_data["zip"]})
                  end

                  if city_data.include?("zipex")
                    postal_code_examples = city_data["zipex"].split(",")
                  end
                end

                if city_data.include?("sub_keys")
                  localized_city_area_choices = make_choices(city_data)
                  city_area_choices += localized_city_area_choices
                  existing_choice = city_area.present?
                  matched_city_area = city_area = match_choices(address.city_area, localized_city_area_choices)
                  if matched_city_area.present?
                    if is_default_language
                      city_area_data = database["#{country_code}/#{country_area}/#{city}/#{city_area}"]
                    else
                      city_area_data = database["#{country_code}/#{country_area}/#{city}/#{city_area}--#{language}"]
                    end

                    if existing_choice.blank?
                      if city_area_data.include?("zip")
                        postal_code_matchers << %r(^#{city_area_data["zip"]})
                      end
                      if city_area_data.include?("zipex")
                        postal_code_examples = city_area_data["zipex"].split(",")
                      end
                    end
                  end
                end
              end
            end
          end
          country_area_choices = compact_choices(country_area_choices)
          city_choices = compact_choices(city_choices)
          city_area_choices = compact_choices(city_area_choices)
        end
      end

      ValidationRules.new(country_code, country_name, address_format, address_latin_format,
                          allowed_fields, required_fields, upper_fields, country_area_type,
                          country_area_choices, city_type, city_choices, city_area_type, city_area_choices,
                          postal_code_type, postal_code_matchers, postal_code_examples, postal_code_prefix)
    end

    def load_validation_data(country_code="all")
      if !VALID_COUNTRY_CODE.match(country_code)
        raise "#{country_code} is not a valid country code"
      end

      path = VALIDATION_DATA_PATH % country_code.downcase
      if !File.exist?(path)
        raise "No metadata file exists at path: #{path}"
      end

      JSON.parse(File.read(path))
    end

    def load_country_data(country_code)
      # This ZZ stuff is just defaults to init with
      database = load_validation_data("zz")
      country_data = database["ZZ"]
      if country_code
        country_code = country_code.upcase
        if country_code.downcase == "zz"
          raise "'#{country_code}' is not a valid country code"
        end
        database = load_validation_data(country_code.downcase)
        country_data.merge!(database[country_code.upcase])
      end
      [country_data, database]
    end
  end
end