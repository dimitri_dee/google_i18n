require "json"
require "active_support/core_ext/object/blank"
require "active_support/ordered_hash"

module GoogleI18n
  class InvalidAddress < StandardError; end
  class Error < StandardError; end

  require "google_i18n/version"
  require "google_i18n/validation_rules"
  require "google_i18n/address"
end
